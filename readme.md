Samagramp
===========================

A mod manager for GTA V that let's you easily disable/reenable/install mods.

WX form built with wxFormBuilder, then edited using an extended class (\_\_init\_\_.py)

![Samagramp](samagramp.PNG)

Dependencies:

- wxFormBuilder
- pyinstaller
- pywin32
- wxPython

# Notes:
- open form.fbp to edit the program GUI. Click "generate code" or F8 in formbuilder to update the form file.

- run compile.bat to compile the program to .exe

- don't edit MainFrame.py, that's edited by form.fbp. (It's where the code is generated to)

- \_\_init\_\_.py is where most editing is done, init.py interacts with the form file through inherited classes

- Run init.py to debug the program without compiling.
