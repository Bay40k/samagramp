# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Samagramp", pos = wx.DefaultPosition, size = wx.Size( 990,540 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.SYSTEM_MENU|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_appTitle = wx.StaticText( self, wx.ID_ANY, u"Samagramp", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_appTitle.Wrap( -1 )

		self.m_appTitle.SetFont( wx.Font( 12, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, "Arial" ) )

		bSizer1.Add( self.m_appTitle, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )

		self.m_appDesc = wx.StaticText( self, wx.ID_ANY, u"GTA V Mod Manager by: Bay40k", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_appDesc.Wrap( -1 )

		self.m_appDesc.SetFont( wx.Font( 9, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_ITALIC, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer1.Add( self.m_appDesc, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )

		self.m_appNote = wx.StaticText( self, wx.ID_ANY, u"Please refer to the console for any unexplained errors.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_appNote.Wrap( -1 )

		bSizer1.Add( self.m_appNote, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )

		gamePath = wx.GridSizer( 1, 2, 0, 0 )

		gSizer5 = wx.GridSizer( 2, 1, 0, 0 )

		self.m_staticText6 = wx.StaticText( self, wx.ID_ANY, u"GTA V File Path (SET THIS!):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )

		self.m_staticText6.SetFont( wx.Font( 8, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		gSizer5.Add( self.m_staticText6, 0, wx.ALL|wx.ALIGN_BOTTOM, 5 )

		self.m_gtaPath = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.Point( -1,-1 ), wx.Size( -1,-1 ), wx.DIRP_DEFAULT_STYLE )
		gSizer5.Add( self.m_gtaPath, 0, wx.ALL|wx.EXPAND, 5 )


		gamePath.Add( gSizer5, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		gSizer6 = wx.GridSizer( 2, 1, 0, 0 )


		gSizer6.Add( ( 0, 0), 1, 0, 5 )

		self.m_setPath = wx.Button( self, wx.ID_ANY, u"Set", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		gSizer6.Add( self.m_setPath, 0, wx.ALL|wx.EXPAND, 5 )


		gamePath.Add( gSizer6, 0, wx.EXPAND|wx.RIGHT, 5 )


		bSizer1.Add( gamePath, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Mods" ), wx.VERTICAL )

		gSizer4 = wx.GridSizer( 1, 2, 0, 0 )

		self.m_installMods = wx.Button( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Install Mods", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_installMods, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_uninstallMods = wx.Button( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Uninstall Mods", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_uninstallMods, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer3.Add( gSizer4, 0, wx.EXPAND, 5 )

		self.m_modDirectory = wx.Button( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Open \"mods\" Directory (Place mods here)", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer3.Add( self.m_modDirectory, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer1.Add( sbSizer3, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_statusBar = self.CreateStatusBar( 1, wx.STB_SIZEGRIP, wx.ID_ANY )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_setPath.Bind( wx.EVT_BUTTON, self.setGTAVPath )
		self.m_installMods.Bind( wx.EVT_BUTTON, self.installMods )
		self.m_uninstallMods.Bind( wx.EVT_BUTTON, self.uninstallMods )
		self.m_modDirectory.Bind( wx.EVT_BUTTON, self.openModDirectory )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def setGTAVPath( self, event ):
		event.Skip()

	def installMods( self, event ):
		event.Skip()

	def uninstallMods( self, event ):
		event.Skip()

	def openModDirectory( self, event ):
		event.Skip()


