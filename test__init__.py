from unittest import TestCase
import os
import wx
from __init__ import ProjectMainFrame


class TestProjectMainFrame(TestCase):
    def __init__(self, *args, **kwargs):
        app = wx.App(False)
        self.test_project_mainframe = ProjectMainFrame(None)
        super().__init__(*args, **kwargs)

    def test_check_files(self):
        os.system("del /Q installedMods.txt")
        os.system("del /Q gamepath.txt")
        self.test_project_mainframe.check_files()
        self.assertEqual(os.path.exists("installedMods.txt"), True)
        self.assertEqual(os.path.exists("gamepath.txt"), True)

    def create_test_mods(self):
        # create mods directory, delete if existing
        if os.path.exists("mods/"):
            os.system('rmdir /S /Q "mods"')
        os.system('mkdir "mods"')

        # verify mods folder is empty
        self.assertEqual(len(os.listdir("mods")), 0)

        # create test files
        os.system('echo "">"mods\\testfile.asi"')
        os.system('mkdir "mods\\testdir"')

    def test_save_mods_to_list(self):
        self.create_test_mods()

        self.test_project_mainframe.save_mods_to_list()

        with open("installedMods.txt", "r+") as f:
            string = f.read()
            file_array = string.split('\n')
            self.assertIn("testdir", file_array)
            self.assertIn("testfile.asi", file_array)
            f.truncate()

        os.system('rmdir /S /Q "mods"')

    def test_copy_mods(self):
        self.create_test_mods()
        if os.path.exists("gtaLoc/"):
            os.system('rmdir /S /Q "gtaLoc"')
        os.system('mkdir "gtaLoc"')

        self.test_project_mainframe.set_path_var("gtaLoc")
        self.test_project_mainframe.copy_mods()

        self.assertEqual(os.path.exists("gtaLoc/testdir"), True)
        self.assertEqual(os.path.exists("gtaLoc/testfile.asi"), True)

        os.system('rmdir /S /Q "mods"')
        os.system('rmdir /S /Q "gtaLoc"')

    def test_delete_mods_on_list(self):
        self.create_test_mods()
        if os.path.exists("gtaLoc/"):
            os.system('rmdir /S /Q "gtaLoc"')
        os.system('mkdir "gtaLoc"')

        self.test_project_mainframe.set_path_var("gtaLoc")
        self.test_project_mainframe.check_files()
        self.test_project_mainframe.save_mods_to_list()
        self.test_project_mainframe.copy_mods()

        self.test_project_mainframe.delete_mods_on_list()

        self.assertEqual(os.path.exists("gtaLoc/testdir"), False)
        self.assertEqual(os.path.exists("gtaLoc/testfile.asi"), False)

        os.system('rmdir /S /Q "mods"')
        os.system('rmdir /S /Q "gtaLoc"')
